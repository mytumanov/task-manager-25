package ru.mtumanov.tm.exception.field;

public class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("ERROR! Login is empty!");
    }

}
