package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId) throws AbstractException;

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws AbstractException;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws AbstractException;

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws AbstractException;

    @NotNull
    M remove(@NotNull String userId, @NotNull M model) throws AbstractException;

    @NotNull
    M removeById(@NotNull String userId, @NotNull String id) throws AbstractException;

    @NotNull
    M removeByIndex(@NotNull String userId, @NotNull Integer index) throws AbstractException;

    void clear(@NotNull String userId) throws AbstractException;

    int getSize(@NotNull String userId) throws AbstractException;

    boolean existById(@NotNull String userId, @NotNull String id) throws AbstractException;

}
