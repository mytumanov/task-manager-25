package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Close application";
    }

    @Override
    @NotNull
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
